package pl.clarite.example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.clarite.example.model.Example;
import pl.clarite.example.model.dto.ExampleDto;
import pl.clarite.example.model.mapper.ModelMapper;
import pl.clarite.example.service.ExampleService;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class ExampleController {

    private ExampleService exampleService;
    private ModelMapper modelMapper;

    public ExampleController(ExampleService exampleService) {
        this.exampleService = exampleService;
    }


    @GetMapping("/example/{id}")
    public ResponseEntity<ExampleDto> findById(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(exampleService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/example/max")
    public ResponseEntity<ExampleDto> findExampleWithMaxBigDecimalValue() {
        try {
            return new ResponseEntity<>(exampleService.findExampleWithMaxBigDecimalValue(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/examples")
    public ResponseEntity<List<ExampleDto>> findAll() {
        return new ResponseEntity<>(exampleService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/example")
    public ResponseEntity<ExampleDto> save(@RequestBody ExampleDto exampleDto) {
        return new ResponseEntity<>(exampleService.save(exampleDto), HttpStatus.CREATED);
    }
}

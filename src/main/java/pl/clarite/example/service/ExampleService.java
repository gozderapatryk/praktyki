package pl.clarite.example.service;

import org.springframework.stereotype.Service;
import pl.clarite.example.model.Example;
import pl.clarite.example.model.dto.ExampleDto;
import pl.clarite.example.model.mapper.ModelMapper;
import pl.clarite.example.repository.ExampleRepository;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class ExampleService {

    private ModelMapper modelMapper;
    private ExampleRepository exampleRepository;

    public ExampleService(ModelMapper modelMapper, ExampleRepository exampleRepository) {
        this.modelMapper = modelMapper;
        this.exampleRepository = exampleRepository;
    }

    public ExampleDto save(ExampleDto exampleDto) {
        return  modelMapper.fromExampleToExampleDto(exampleRepository.save(modelMapper.fromExampleDtoToExample(exampleDto)));
    }

    public ExampleDto findById(Long id) throws NoSuchElementException {
        return modelMapper.fromExampleToExampleDto(exampleRepository.findById(id).orElseThrow(NoSuchElementException::new));
    }

    public ExampleDto findExampleWithMaxBigDecimalValue() throws NoSuchElementException {
        return  modelMapper.fromExampleToExampleDto(exampleRepository.findAll().stream().max(Comparator.comparing(Example::getSomeInteger).thenComparing(Example::getSomeBigDecimal)).orElseThrow(NoSuchElementException::new));
    }

    public List<ExampleDto> findAll() {
        return exampleRepository.findAll()
                .stream()
                .map(modelMapper::fromExampleToExampleDto)
                .collect(Collectors.toList());
    }
}

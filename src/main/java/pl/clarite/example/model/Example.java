package pl.clarite.example.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "examples")
public class Example {
    @Id
    @GeneratedValue
    private Long id;
    private String someString;
    private Integer someInteger;
    private BigDecimal someBigDecimal;
}

package pl.clarite.example.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExampleDto {
    private Long id;
    private String someString;
    private Integer someInteger;
    private BigDecimal someBigDecimal;
}

package pl.clarite.example.model.mapper;

import org.springframework.stereotype.Component;
import pl.clarite.example.model.Example;
import pl.clarite.example.model.dto.ExampleDto;

@Component
public class ModelMapper {

    public Example fromExampleDtoToExample(ExampleDto exampleDto) {
        return exampleDto == null ? null : Example.builder()
                .id(exampleDto.getId())
                .someString(exampleDto.getSomeString())
                .someInteger(exampleDto.getSomeInteger())
                .someBigDecimal(exampleDto.getSomeBigDecimal())
                .build();
    }

    public ExampleDto fromExampleToExampleDto(Example example) {
        return example == null ? null : ExampleDto.builder()
                .id(example.getId())
                .someString(example.getSomeString())
                .someInteger(example.getSomeInteger())
                .someBigDecimal(example.getSomeBigDecimal())
                .build();
    }
}
